namespace RC.Core.Dal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RC.Core.Dal.DatabaseContext.RcContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RC.Core.Dal.DatabaseContext.RcContext context)
        {
            // Get single account data
            //context.Database.ExecuteSqlCommand(
            //    @"DROP PROCEDURE [dbo].[Account_Data];");

            //context.Database.ExecuteSqlCommand(
            //    @"CREATE procedure [dbo].[Account_Data]
            //        @Username nvarchar(255)
            //    AS
            //    SELECT a.Id, a.Username, a.FirstName, a.LastName, a.Password
            //    FROM Account a 
            //    WHERE a.UserName = @UserName");

            // Get accounts
            //context.Database.ExecuteSqlCommand(
            //    @"DROP PROCEDURE [dbo].[Account_List_Data];");

            //context.Database.ExecuteSqlCommand(
            //    @"CREATE procedure [dbo].[Account_List_Data]
            //        @SearchParam nvarchar(255),
            //        @Skip int,
            //        @Take int
            //    AS
            //    SELECT a.Id, a.Username, a.FirstName, a.LastName, a.Password
            //    FROM Account a 
            //    WHERE (((a.Username like '%'+IsNull(@SearchParam,a.Username)+'%'))
            //    OR ((a.FirstName like '%'+IsNull(@SearchParam,a.FirstName)+'%'))
            //    OR ((a.LastName like '%'+IsNull(@SearchParam,a.LastName)+'%')))
            //    ORDER BY a.LastName, a.FirstName
            //    OFFSET @Skip ROWS
            //    FETCH NEXT @Take ROWS ONLY;");

            // Get messages for account
            //    context.Database.ExecuteSqlCommand(
            //        @"DROP PROCEDURE [dbo].[Messages_For_Account];");

            //    context.Database.ExecuteSqlCommand(
            //        @"CREATE procedure [dbo].[Messages_For_Account]
            //            @OwnerId uniqueidentifier,
            //@WithAccountId uniqueidentifier,
            //            @Skip int,
            //            @Take int
            //       AS
            //                DECLARE @AccountId NVARCHAR(36)
            //                SET @AccountId = CAST(@WithAccountId AS NVARCHAR(36))

            //    SELECT m.Id, m.Text, m.FromAccountId, CONCAT(mf.FirstName,' ', mf.LastName) as FromFullName, m.ToAccountId, CONCAT(mt.FirstName,' ', mt.LastName) as ToFullName, m.CreatedOn as SentOn, m.IsRead
            //                FROM Message m 
            //    inner join Account mf on mf.Id = m.FromAccountId
            //    inner join Account mt on mt.Id = m.ToAccountId
            //    WHERE m.OwnerId = @OwnerId
            //    AND m.IsDeleted = 0
            //    AND (((mf.Id like '%' + IsNull(@AccountId,mf.Id) + '%'))
            //                OR ((mt.Id like '%' + IsNull(@AccountId,mt.Id) + '%')))
            //                ORDER BY m.CreatedOn DESC
            //                OFFSET @Skip ROWS
            //                FETCH NEXT @Take ROWS ONLY");


            // Get message unread count
            //context.Database.ExecuteSqlCommand(
            //    @"DROP PROCEDURE [dbo].[Messages_Unread];");

            //context.Database.ExecuteSqlCommand(
            //    @"CREATE procedure [dbo].[Messages_Unread]
            //                  @OwnerId uniqueidentifier
            //            AS
            //            SELECT Count(*) as UnreadCount
            //            FROM Message m 
            //WHERE m.OwnerId = @OwnerId
            //AND m.IsRead = 0
            //AND m.IsDeleted = 0");

            // Flag messages as deleted
            //      context.Database.ExecuteSqlCommand(
            //          @"DROP PROCEDURE [dbo].[Messages_Flag_Deleted];");

            //      //context.Database.ExecuteSqlCommand("CREATE TYPE GuidList AS TABLE (Id uniqueidentifier);");
            //      context.Database.ExecuteSqlCommand(
            //          @"CREATE procedure [dbo].[Messages_Flag_Deleted]
            //	  @IdList GuidList READONLY,
            //                        @OwnerId uniqueidentifier,
            //                        @Username nvarchar(255)

            //                  AS
            //                  UPDATE Message
            //SET IsDeleted = 1, DeletedBy = @Username, DeletedOn = GETDATE()
            //WHERE Id IN (SELECT Id FROM @IdList)
            //                  AND OwnerId = @OwnerId;");

            // Flag messages as read
            context.Database.ExecuteSqlCommand(
                @"DROP PROCEDURE [dbo].[Messages_Flag_Read];");

            //context.Database.ExecuteSqlCommand("CREATE TYPE GuidList AS TABLE (Id uniqueidentifier);");
            context.Database.ExecuteSqlCommand(
                @"CREATE procedure [dbo].[Messages_Flag_Read]
            	  @IdList GuidList READONLY,
                                    @OwnerId uniqueidentifier,
                                    @Username nvarchar(255)
                              AS
                              UPDATE Message
            SET IsRead = 1, ModifiedBy = @Username, ModifiedOn = GETDATE()
            WHERE Id IN (SELECT Id FROM @IdList)
                              AND OwnerId = @OwnerId;");
        }
    }
}
