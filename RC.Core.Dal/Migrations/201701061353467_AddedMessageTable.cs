namespace RC.Core.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMessageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Message",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FromAccountId = c.Guid(nullable: false),
                        ToAccountId = c.Guid(nullable: false),
                        OwnerId = c.Guid(nullable: false),
                        Text = c.String(),
                        IsRead = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 255),
                        ModifiedBy = c.String(maxLength: 255),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedBy = c.String(maxLength: 255),
                        DeletedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.FromAccountId)
                .ForeignKey("dbo.Account", t => t.OwnerId)
                .ForeignKey("dbo.Account", t => t.ToAccountId)
                .Index(t => t.FromAccountId)
                .Index(t => t.ToAccountId)
                .Index(t => t.OwnerId)
                .Index(t => t.ModifiedOn);
            
            DropColumn("dbo.Account", "IsArchived");
            DropColumn("dbo.Account", "ArchivedBy");
            DropColumn("dbo.Account", "ArchivedOn");
            CreateStoredProcedure(
                "dbo.Message_Insert",
                p => new
                    {
                        Id = p.Guid(),
                        FromAccountId = p.Guid(),
                        ToAccountId = p.Guid(),
                        OwnerId = p.Guid(),
                        Text = p.String(),
                        IsRead = p.Boolean(),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                        IsDeleted = p.Boolean(),
                        DeletedBy = p.String(maxLength: 255),
                        DeletedOn = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[Message]([Id], [FromAccountId], [ToAccountId], [OwnerId], [Text], [IsRead], [CreatedOn], [ModifiedOn], [CreatedBy], [ModifiedBy], [IsDeleted], [DeletedBy], [DeletedOn])
                      VALUES (@Id, @FromAccountId, @ToAccountId, @OwnerId, @Text, @IsRead, @CreatedOn, @ModifiedOn, @CreatedBy, @ModifiedBy, @IsDeleted, @DeletedBy, @DeletedOn)"
            );
            
            CreateStoredProcedure(
                "dbo.Message_Update",
                p => new
                    {
                        Id = p.Guid(),
                        FromAccountId = p.Guid(),
                        ToAccountId = p.Guid(),
                        OwnerId = p.Guid(),
                        Text = p.String(),
                        IsRead = p.Boolean(),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                        IsDeleted = p.Boolean(),
                        DeletedBy = p.String(maxLength: 255),
                        DeletedOn = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[Message]
                      SET [FromAccountId] = @FromAccountId, [ToAccountId] = @ToAccountId, [OwnerId] = @OwnerId, [Text] = @Text, [IsRead] = @IsRead, [CreatedOn] = @CreatedOn, [ModifiedOn] = @ModifiedOn, [CreatedBy] = @CreatedBy, [ModifiedBy] = @ModifiedBy, [IsDeleted] = @IsDeleted, [DeletedBy] = @DeletedBy, [DeletedOn] = @DeletedOn
                      WHERE ([Id] = @Id)"
            );
            
            CreateStoredProcedure(
                "dbo.Message_Delete",
                p => new
                    {
                        Id = p.Guid(),
                    },
                body:
                    @"DELETE [dbo].[Message]
                      WHERE ([Id] = @Id)"
            );
            
            AlterStoredProcedure(
                "dbo.Account_Insert",
                p => new
                    {
                        Id = p.Guid(),
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        FirstName = p.String(maxLength: 255),
                        LastName = p.String(maxLength: 255),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                    },
                body:
                    @"INSERT [dbo].[Account]([Id], [Username], [Password], [FirstName], [LastName], [CreatedOn], [ModifiedOn], [CreatedBy], [ModifiedBy])
                      VALUES (@Id, @Username, @Password, @FirstName, @LastName, @CreatedOn, @ModifiedOn, @CreatedBy, @ModifiedBy)"
            );
            
            AlterStoredProcedure(
                "dbo.Account_Update",
                p => new
                    {
                        Id = p.Guid(),
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        FirstName = p.String(maxLength: 255),
                        LastName = p.String(maxLength: 255),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                    },
                body:
                    @"UPDATE [dbo].[Account]
                      SET [Username] = @Username, [Password] = @Password, [FirstName] = @FirstName, [LastName] = @LastName, [CreatedOn] = @CreatedOn, [ModifiedOn] = @ModifiedOn, [CreatedBy] = @CreatedBy, [ModifiedBy] = @ModifiedBy
                      WHERE ([Id] = @Id)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Message_Delete");
            DropStoredProcedure("dbo.Message_Update");
            DropStoredProcedure("dbo.Message_Insert");
            AddColumn("dbo.Account", "ArchivedOn", c => c.DateTime());
            AddColumn("dbo.Account", "ArchivedBy", c => c.String(maxLength: 255));
            AddColumn("dbo.Account", "IsArchived", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Message", "ToAccountId", "dbo.Account");
            DropForeignKey("dbo.Message", "OwnerId", "dbo.Account");
            DropForeignKey("dbo.Message", "FromAccountId", "dbo.Account");
            DropIndex("dbo.Message", new[] { "ModifiedOn" });
            DropIndex("dbo.Message", new[] { "OwnerId" });
            DropIndex("dbo.Message", new[] { "ToAccountId" });
            DropIndex("dbo.Message", new[] { "FromAccountId" });
            DropTable("dbo.Message");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
