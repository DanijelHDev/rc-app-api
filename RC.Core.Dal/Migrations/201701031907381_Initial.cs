namespace RC.Core.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Username = c.String(maxLength: 255),
                        Password = c.String(),
                        FirstName = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 255),
                        ModifiedBy = c.String(maxLength: 255),
                        IsArchived = c.Boolean(nullable: false),
                        ArchivedBy = c.String(maxLength: 255),
                        ArchivedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ModifiedOn);
            
            CreateStoredProcedure(
                "dbo.Account_Insert",
                p => new
                    {
                        Id = p.Guid(),
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        FirstName = p.String(maxLength: 255),
                        LastName = p.String(maxLength: 255),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                        IsArchived = p.Boolean(),
                        ArchivedBy = p.String(maxLength: 255),
                        ArchivedOn = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[Account]([Id], [Username], [Password], [FirstName], [LastName], [CreatedOn], [ModifiedOn], [CreatedBy], [ModifiedBy], [IsArchived], [ArchivedBy], [ArchivedOn])
                      VALUES (@Id, @Username, @Password, @FirstName, @LastName, @CreatedOn, @ModifiedOn, @CreatedBy, @ModifiedBy, @IsArchived, @ArchivedBy, @ArchivedOn)"
            );
            
            CreateStoredProcedure(
                "dbo.Account_Update",
                p => new
                    {
                        Id = p.Guid(),
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        FirstName = p.String(maxLength: 255),
                        LastName = p.String(maxLength: 255),
                        CreatedOn = p.DateTime(),
                        ModifiedOn = p.DateTime(),
                        CreatedBy = p.String(maxLength: 255),
                        ModifiedBy = p.String(maxLength: 255),
                        IsArchived = p.Boolean(),
                        ArchivedBy = p.String(maxLength: 255),
                        ArchivedOn = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[Account]
                      SET [Username] = @Username, [Password] = @Password, [FirstName] = @FirstName, [LastName] = @LastName, [CreatedOn] = @CreatedOn, [ModifiedOn] = @ModifiedOn, [CreatedBy] = @CreatedBy, [ModifiedBy] = @ModifiedBy, [IsArchived] = @IsArchived, [ArchivedBy] = @ArchivedBy, [ArchivedOn] = @ArchivedOn
                      WHERE ([Id] = @Id)"
            );
            
            CreateStoredProcedure(
                "dbo.Account_Delete",
                p => new
                    {
                        Id = p.Guid(),
                    },
                body:
                    @"DELETE [dbo].[Account]
                      WHERE ([Id] = @Id)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Account_Delete");
            DropStoredProcedure("dbo.Account_Update");
            DropStoredProcedure("dbo.Account_Insert");
            DropIndex("dbo.Account", new[] { "ModifiedOn" });
            DropTable("dbo.Account");
        }
    }
}
