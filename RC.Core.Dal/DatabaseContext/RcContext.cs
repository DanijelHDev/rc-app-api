﻿using RC.Core.Entity.Common;
using RC.Core.Entity.Configurations;
using RC.Core.Entity.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RC.Core.Dal.DatabaseContext
{
    public class RcContextFactory : IDbContextFactory<RcContext>
    {
        public RcContext Create()
        {
            return new RcContext(ConnectionStrings.StagingEnvironment);
        }
    }

    public class DbConfig : DbConfiguration
    {
        public DbConfig()
        {
            SetProviderServices("System.Data.SqlClient", SqlProviderServices.Instance);
        }
    }

    [DbConfigurationType(typeof(DbConfig))]
    public class RcContext : DbContext
    {
        public RcContext(string connString) : base(connString)
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
#if DEBUG
            this.Database.Log = s => Debug.Write(s);
#endif
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Message> Messages{ get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new AccountConfiguration());
            modelBuilder.Configurations.Add(new MessageConfiguration());
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => (x.Entity is IAuditableWithSoftDeleteEntity || x.Entity is IAuditableEntity)
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var prop in modifiedEntries)
            {
                IAuditableWithSoftDeleteEntity entityAuditableWithArchive = prop.Entity as IAuditableWithSoftDeleteEntity;
                if (entityAuditableWithArchive != null)
                {
                    string identityAccountName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(identityAccountName))
                            entityAuditableWithArchive.CreatedBy = identityAccountName;

                        entityAuditableWithArchive.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(identityAccountName))
                        entityAuditableWithArchive.ModifiedBy = identityAccountName;

                    entityAuditableWithArchive.ModifiedOn = dateNow;
                }

                IAuditableEntity entityAuditable = prop.Entity as IAuditableEntity;
                if (entityAuditable != null)
                {
                    string identityAccountName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(identityAccountName))
                            entityAuditable.CreatedBy = identityAccountName;

                        entityAuditable.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditable).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditable).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(identityAccountName))
                        entityAuditable.ModifiedBy = identityAccountName;

                    entityAuditable.ModifiedOn = dateNow;
                }
            }

            return base.SaveChanges();
        }

        public int SaveChanges(string currentAccountUsername)
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => (x.Entity is IAuditableWithSoftDeleteEntity || x.Entity is IAuditableEntity)
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var prop in modifiedEntries)
            {
                IAuditableWithSoftDeleteEntity entityAuditableWithArchive = prop.Entity as IAuditableWithSoftDeleteEntity;
                if (entityAuditableWithArchive != null)
                {
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(currentAccountUsername))
                            entityAuditableWithArchive.CreatedBy = currentAccountUsername;

                        entityAuditableWithArchive.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(currentAccountUsername))
                        entityAuditableWithArchive.ModifiedBy = currentAccountUsername;

                    entityAuditableWithArchive.ModifiedOn = dateNow;
                }

                IAuditableEntity entityAuditable = prop.Entity as IAuditableEntity;
                if (entityAuditable != null)
                {
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(currentAccountUsername))
                            entityAuditable.CreatedBy = currentAccountUsername;

                        entityAuditable.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditable).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditable).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(currentAccountUsername))
                        entityAuditable.ModifiedBy = currentAccountUsername;

                    entityAuditable.ModifiedOn = dateNow;
                }
            }

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => (x.Entity is IAuditableWithSoftDeleteEntity || x.Entity is IAuditableEntity) && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var prop in modifiedEntries)
            {
                IAuditableWithSoftDeleteEntity entityAuditableWithArchive = prop.Entity as IAuditableWithSoftDeleteEntity;
                if (entityAuditableWithArchive != null)
                {
                    var identityAccountName = Thread.CurrentPrincipal.Identity.Name;
                    var dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(identityAccountName))
                            entityAuditableWithArchive.CreatedBy = identityAccountName;

                        entityAuditableWithArchive.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(identityAccountName))
                        entityAuditableWithArchive.ModifiedBy = identityAccountName;

                    entityAuditableWithArchive.ModifiedOn = dateNow;
                }

                IAuditableEntity entityAuditable = prop.Entity as IAuditableEntity;
                if (entityAuditable != null)
                {
                    string identityAccountName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(identityAccountName))
                            entityAuditable.CreatedBy = identityAccountName;

                        entityAuditable.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditable).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditable).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(identityAccountName))
                        entityAuditable.ModifiedBy = identityAccountName;

                    entityAuditable.ModifiedOn = dateNow;
                }
            }

            return await base.SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync(string currentAccountUsername)
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => (x.Entity is IAuditableWithSoftDeleteEntity || x.Entity is IAuditableEntity)
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var prop in modifiedEntries)
            {
                IAuditableWithSoftDeleteEntity entityAuditableWithArchive = prop.Entity as IAuditableWithSoftDeleteEntity;
                if (entityAuditableWithArchive != null)
                {
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(currentAccountUsername))
                            entityAuditableWithArchive.CreatedBy = currentAccountUsername;

                        entityAuditableWithArchive.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditableWithArchive).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(currentAccountUsername))
                        entityAuditableWithArchive.ModifiedBy = currentAccountUsername;

                    entityAuditableWithArchive.ModifiedOn = dateNow;
                }

                IAuditableEntity entityAuditable = prop.Entity as IAuditableEntity;
                if (entityAuditable != null)
                {
                    DateTime dateNow = DateTime.UtcNow;

                    if (prop.State == System.Data.Entity.EntityState.Added)
                    {
                        if (!String.IsNullOrEmpty(currentAccountUsername))
                            entityAuditable.CreatedBy = currentAccountUsername;

                        entityAuditable.CreatedOn = dateNow;
                    }
                    else
                    {
                        base.Entry(entityAuditable).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entityAuditable).Property(x => x.CreatedOn).IsModified = false;
                    }

                    if (!String.IsNullOrEmpty(currentAccountUsername))
                        entityAuditable.ModifiedBy = currentAccountUsername;

                    entityAuditable.ModifiedOn = dateNow;
                }
            }

            return await base.SaveChangesAsync();
        }
    }
}
