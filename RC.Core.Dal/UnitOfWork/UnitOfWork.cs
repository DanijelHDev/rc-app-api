﻿using RC.Core.Dal.DatabaseContext;
using RC.Core.Dal.Repositories;
using RC.Core.Entity.Common;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace RC.Core.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RcContext _dbContext;

        public UnitOfWork()
        {
            _dbContext = new RcContext(ConnectionStrings.StagingEnvironment);

            Accounts = new AccountRepository(_dbContext);
            Messages = new MessageRepository(_dbContext);
        }

        public int ExecuteUpdates() => _dbContext.SaveChanges();

        public int ExecuteUpdates(string currentAccountUsername) => _dbContext.SaveChanges(currentAccountUsername);

        public async Task<int> ExecuteUpdatesAsync() => await _dbContext.SaveChangesAsync();

        public async Task<int> ExecuteUpdatesAsync(string currentAccountUsername) => await _dbContext.SaveChangesAsync(currentAccountUsername);

        public DbRawSqlQuery<T> SqlQuery<T>(string sql, params object[] parameters)
        {
            return _dbContext.Database.SqlQuery<T>(sql, parameters);
        }

        public async Task SqlCommand(string sql, params object[] parameters)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync(sql, parameters);
        }

        public void Dispose() => _dbContext.Dispose();

        public IAccountRepository Accounts { get; private set; }
        public IMessageRepository Messages { get; private set; }
    }
}