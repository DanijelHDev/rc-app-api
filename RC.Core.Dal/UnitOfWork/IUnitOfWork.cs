﻿using RC.Core.Dal.Repositories;
using System;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace RC.Core.DataAccess.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IAccountRepository Accounts { get; }
        IMessageRepository Messages { get; }

        int ExecuteUpdates();

        /// <summary>
        /// Pass in username from ICurrentAccount interface
        /// </summary>
        /// <param name="currentAccountUsername"></param>
        /// <returns></returns>
        int ExecuteUpdates(string currentAccountUsername);

        Task<int> ExecuteUpdatesAsync();

        /// <summary>
        /// Pass in username from ICurrentAccount interface
        /// </summary>
        /// <param name="currentAccountUsername"></param>
        /// <returns></returns>
        Task<int> ExecuteUpdatesAsync(string currentAccountUsername);

        /// <summary>
        /// Execute raw SQL query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        DbRawSqlQuery<T> SqlQuery<T>(string sql, params object[] parameters);

        /// <summary>
        /// Execute raw SQL query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task SqlCommand(string sql, params object[] parameters);
    }
}