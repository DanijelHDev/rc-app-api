﻿using RC.Core.Dal.DatabaseContext;
using RC.Core.Entity.Entities;
using System.Data.Entity;

namespace RC.Core.Dal.Repositories
{
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public DbContext DatabaseContext => DbContext as RcContext;
        public AccountRepository(RcContext dbContext) : base(dbContext) { }
    }
}
