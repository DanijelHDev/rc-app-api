﻿using RC.Core.Entity.Entities;

namespace RC.Core.Dal.Repositories
{
    public interface IAccountRepository : IBaseRepository<Account>
    {
    }
}
