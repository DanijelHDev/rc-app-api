﻿using RC.Core.Dal.DatabaseContext;
using RC.Core.Entity.Entities;
using System.Data.Entity;

namespace RC.Core.Dal.Repositories
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        public DbContext DatabaseContext => DbContext as RcContext;
        public MessageRepository(RcContext dbContext) : base(dbContext) { }
    }
}
