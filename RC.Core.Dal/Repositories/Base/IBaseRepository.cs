﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RC.Core.Dal.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAllQueryable();
        IQueryable<TEntity> GetAllQueryableNoTracking();
        IEnumerable<TEntity> GetAllEnumerable();
        IEnumerable<TEntity> GetAllEnumerableNoTracking();

        void AttachEntity(TEntity entity);
        void SetEntityState(TEntity entity, EntityState entityState);
        
        TEntity Find(params object[] keys);
        Task<TEntity> FindAsync(params object[] keys);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        void AddToDatabase(TEntity entity);
        void AddOrUpdateInDatabase(TEntity entity, int id);
        void AddOrUpdateInDatabase(TEntity entity, Guid id);
        void AddRangeToDatabase(IEnumerable<TEntity> entities);
        void UpdateInDatabase(TEntity entity, int id);
        void UpdateInDatabase(TEntity entity, Guid id);
        void DeleteFromDatabase(int id);
        void DeleteFromDatabase(Guid id);
        void DeleteFromDatabase(TEntity entity);
        void DeleteRangeFromDatabase(IEnumerable<TEntity> entities);

        void ExecuteUpdates();
        Task ExecuteUpdatesAsync();
    }
}
