﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RC.Core.Dal.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _entities;
        protected readonly DbContext DbContext;

        public BaseRepository(DbContext dbContext)
        {
            DbContext = dbContext;
            _entities = DbContext.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAllEnumerable() => _entities.AsEnumerable();
        public IEnumerable<TEntity> GetAllEnumerableNoTracking() => _entities.AsNoTracking().AsEnumerable();

        public IQueryable<TEntity> GetAllQueryable() => _entities.AsQueryable();
        public IQueryable<TEntity> GetAllQueryableNoTracking() => _entities.AsNoTracking().AsQueryable();

        public void AttachEntity(TEntity entity) => _entities.Attach(entity);
        public void SetEntityState(TEntity entity, EntityState entityState) => DbContext.Entry(entity).State = entityState;
        
        public TEntity Find(params object[] keys)
        {
            return _entities.Find(keys);
        }

        public async Task<TEntity> FindAsync(params object[] keys)
        {
            return await _entities.FindAsync(keys);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _entities.FirstOrDefault(predicate);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _entities.FirstOrDefaultAsync(predicate);
        }

        public void AddToDatabase(TEntity entity)
        {
            _entities.Add(entity);
        }

        public void AddOrUpdateInDatabase(TEntity entity, int id)
        {
            TEntity baseEntity = _entities.Find(id);
            if (baseEntity == null)
                AddToDatabase(entity);
            else
                UpdateInDatabase(entity, id);
        }

        public void AddOrUpdateInDatabase(TEntity entity, Guid id)
        {
            TEntity baseEntity = _entities.Find(id);
            if (baseEntity == null)
                AddToDatabase(entity);
            else
                DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);
        }

        public void AddRangeToDatabase(IEnumerable<TEntity> entities)
        {
            _entities.AddRange(entities);
        }

        public void UpdateInDatabase(TEntity entity, int id)
        {
            var baseEntity = _entities.Find(id);
            DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);
        }

        public void UpdateInDatabase(TEntity entity, Guid id)
        {
            var baseEntity = _entities.Find(id);
            DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);
        }

        public void DeleteFromDatabase(int id)
        {
            var entity = _entities.Find(id);
            _entities.Remove(entity);
        }

        public void DeleteFromDatabase(Guid id)
        {
            var entity = _entities.Find(id);
            _entities.Remove(entity);
        }

        public void DeleteFromDatabase(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public void DeleteRangeFromDatabase(IEnumerable<TEntity> entities)
        {
            _entities.RemoveRange(entities);
        }

        public void ExecuteUpdates()
        {
            DbContext.SaveChanges();
        }

        public async Task ExecuteUpdatesAsync()
        {
            await DbContext.SaveChangesAsync();
        }
    }
}
