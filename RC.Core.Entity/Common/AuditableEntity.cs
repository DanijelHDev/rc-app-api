﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RC.Core.Entity.Common
{
    public class AuditableEntity<T> : Entity<T>, IAuditableEntity
    {
        public DateTime CreatedOn { get; set; }

        [Index("IX_ModifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [MaxLength(255)]
        public string CreatedBy { get; set; }

        [MaxLength(255)]
        public string ModifiedBy { get; set; }
    }
}
