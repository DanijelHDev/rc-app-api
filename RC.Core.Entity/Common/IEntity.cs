﻿namespace RC.Core.Entity.Common
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
