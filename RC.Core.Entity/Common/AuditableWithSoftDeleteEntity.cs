﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RC.Core.Entity.Common
{
    public class AuditableWithSoftDeleteEntity<T> : Entity<T>, IAuditableWithSoftDeleteEntity, IDeleted
    {
        public DateTime CreatedOn { get; set; }

        [Index("IX_ModifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [MaxLength(255)]
        public string CreatedBy { get; set; }

        [MaxLength(255)]
        public string ModifiedBy { get; set; }

        public bool IsDeleted { get; set; } = false;

        [MaxLength(255)]
        public string DeletedBy { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
