﻿using System;

namespace RC.Core.Entity.Common
{
    public abstract class BaseEntity
    {
    }

    public class Entity<T> : BaseEntity, IEntity<T>
    {
        public T Id { get; set; }
    }
}