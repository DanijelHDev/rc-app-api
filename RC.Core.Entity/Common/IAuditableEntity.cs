﻿using System;

namespace RC.Core.Entity.Common
{
    public interface IAuditableEntity
    {
        DateTime CreatedOn { get; set; }
        DateTime ModifiedOn { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
    }
}
