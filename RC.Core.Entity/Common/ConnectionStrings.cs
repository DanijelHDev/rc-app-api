﻿namespace RC.Core.Entity.Common
{
    public static class ConnectionStrings
    {
        private const string Development = @"Server=tcp:rc-sql-server.database.windows.net,1433;Initial Catalog=rc-dev-db;Persist Security Info=False;User ID=rcsysadmin;Password=Pass?Ok.Pass!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        public static string StagingEnvironment
        {
            get
            {
                return Development;
            }
        }
    }
}
