﻿using System;

namespace RC.Core.Entity.Common
{
    public interface IAuditableWithSoftDeleteEntity
    {
        DateTime CreatedOn { get; set; }

        DateTime ModifiedOn { get; set; }

        string CreatedBy { get; set; }

        string ModifiedBy { get; set; }

        bool IsDeleted { get; set; }

        string DeletedBy { get; set; }

        DateTime? DeletedOn { get; set; }
    }
}