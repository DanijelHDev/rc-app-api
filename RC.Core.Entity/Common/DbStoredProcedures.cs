﻿
namespace RC.Core.Entity.Common
{
    public static class DbStoredProcedures
    {
        public const string AccountData = "Account_Data";
        public const string AccountListData = "Account_List_Data";
        public const string MessagesForAccount = "Messages_For_Account";
        public const string MessagesUnread = "Messages_Unread";
        public const string MessagesFlagDeleted = "Messages_Flag_Deleted";
        public const string MessagesFlagRead = "Messages_Flag_Read";
    }
}
