﻿using RC.Core.Entity.Common;
using System;

namespace RC.Core.Entity.Entities
{
    public class Message : AuditableWithSoftDeleteEntity<Guid>
    {
        public Guid FromAccountId { get; set; }
        public virtual Account FromAccount { get; set; }

        public Guid ToAccountId { get; set; }
        public virtual Account ToAccount { get; set; }

        public Guid OwnerId { get; set; }
        public virtual Account Owner{ get; set; }
        
        public string Text { get; set; }

        public bool IsRead { get; set; } = false;
    }
}
