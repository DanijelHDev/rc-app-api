﻿using RC.Core.Entity.Entities;
using System.Data.Entity.ModelConfiguration;

namespace RC.Core.Entity.Configurations
{
    public class AccountConfiguration : EntityTypeConfiguration<Account>
    {
        public AccountConfiguration()
        {
            HasKey(a => a.Id);

            Property(a => a.Username).HasMaxLength(255);
            Property(a => a.FirstName).HasMaxLength(255);
            Property(a => a.LastName).HasMaxLength(255);

            MapToStoredProcedures();
        }
    }
}
