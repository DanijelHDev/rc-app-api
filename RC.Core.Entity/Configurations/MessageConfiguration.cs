﻿using RC.Core.Entity.Entities;
using System.Data.Entity.ModelConfiguration;

namespace RC.Core.Entity.Configurations
{
    public class MessageConfiguration : EntityTypeConfiguration<Message>
    {
        public MessageConfiguration()
        {
            HasKey(m => m.Id);
            
            Property(m => m.Text).IsMaxLength();

            HasRequired(m => m.FromAccount)
                .WithMany(a => a.SentMessages)
                .HasForeignKey(m => m.FromAccountId)
                .WillCascadeOnDelete(false);

            HasRequired(m => m.ToAccount)
                .WithMany(a => a.ReceivedMessages)
                .HasForeignKey(m => m.ToAccountId)
                .WillCascadeOnDelete(false);

            HasRequired(m => m.Owner)
                .WithMany(a => a.AccountMessages)
                .HasForeignKey(m => m.OwnerId)
                .WillCascadeOnDelete(false);
            
            MapToStoredProcedures();
        }
    }
}
