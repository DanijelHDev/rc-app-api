﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace RC.App.Api.Controllers.Api
{
    [Authorize]
    [Route("api/[controller]")]
    public class ErrorController : Controller
    {
        [AllowAnonymous]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("/error/{statusCode}")]
        public IActionResult ErrorRoute(string statusCode)
        {
            int code;

            if (int.TryParse(statusCode, out code))
                return StatusCode(code);

            return StatusCode(500);
        }
    }
}
