﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Api.Models.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using RC.Bll.Api.Services.Authentication;
using RC.Bll.Api.Helpers;
using RC.Bll.Api.Models.HttpResult;
using System.Net;
using RC.Bll.Api.Models.Request;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.App.Api.Controllers.Api
{
    [Route(ApiConstants.ApiRoutes.Base.BaseRoute)]
    public class TokenController : Controller
    {
        private IRcAuthentication _authenticationService;
        private readonly JwtTokenIssuerOptions _tokenOptions;

        public TokenController(IRcAuthentication authenticationService)
        {
            _authenticationService = authenticationService;

            _tokenOptions = new JwtTokenIssuerOptions()
            {
                Issuer = ApiConstants.ApiSettings.Issuer,
                Audience = ApiConstants.ApiSettings.Audience,
                SigningCredentials = ApiConstants.ApiSettings.SigningCredentials
            };
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Route(ApiConstants.ApiRoutes.TokenRoutes.TokenEndpoint)]
        public async Task<IActionResult> GenerateToken([FromBody]TokenRequest request)
        {
            if(String.IsNullOrEmpty(request.Username) || String.IsNullOrEmpty(request.Password))
                return new RcHttpResult(
                    HttpStatusCode.BadRequest,
                    ErrorHandler.CreateErrorResponse("Username and password are required"))
                    .Value;

            var identity = await _authenticationService.GetAccountIdentity(request.Username, request.Password);
            if (identity == null)
                return new RcHttpResult(
                    HttpStatusCode.BadRequest,
                    ErrorHandler.CreateErrorResponse("Invalid credentials"))
                    .Value;

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, request.Username),
                new Claim(JwtRegisteredClaimNames.Jti, await _tokenOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, _tokenOptions.IssuedAt.ToUnixEpochDate().ToString(), ClaimValueTypes.Integer64),
            };

            identity.Claims.ToList().ForEach(claim => claims.Add(claim));

            var jwt = new JwtSecurityToken(
                issuer: _tokenOptions.Issuer,
                audience: _tokenOptions.Audience,
                claims: claims,
                notBefore: _tokenOptions.NotBefore,
                expires: _tokenOptions.Expiration,
                signingCredentials: _tokenOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                Id = identity.Claims.FirstOrDefault(c => c.Type == WebConstants.AccountClaims.Id)?.Value,
                Username = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value,
                FullName = identity.Claims.FirstOrDefault(c => c.Type == WebConstants.AccountClaims.FullName)?.Value,
                AccessToken = encodedJwt,
                ExpiresIn = (int)_tokenOptions.ValidFor.TotalSeconds
            };

            return Ok(response);
        }

        private static void ThrowIfInvalidOptions(JwtTokenIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtTokenIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtTokenIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtTokenIssuerOptions.JtiGenerator));
            }
        }
    }
}
