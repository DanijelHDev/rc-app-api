﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Api.Models.Request;
using Microsoft.AspNetCore.Authorization;
using RC.Bll.Api.Services.Accounts;
using System.Net;
using RC.Bll.Api.Models.Validation;
using RC.Bll.Api.Models.HttpResult;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.App.Api.Controllers.Api
{
    [Authorize]
    [Route(ApiConstants.ApiRoutes.Base.BaseRoute)]
    public class AccountController : Controller
    {
        private IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route(ApiConstants.ApiRoutes.AccountRoutes.Register)]
        public async Task<IActionResult> RegisterAccount([FromBody]RegisterRequest request)
        {
            var validator = new RegisterRequestValidation();
            var results = validator.Validate(request);
            if (!results.IsValid)
            {
                return new RcHttpResult(
                    HttpStatusCode.BadRequest,
                    ErrorHandler.CreateErrorResponse(results.Errors.Select(e => e.ErrorMessage).ToList()))
                    .Value;
            }

            var response = await _accountService.Register(request);

            if (response.Value.StatusCode != (int)HttpStatusCode.OK)
                return response.Value;

            return Ok();
        }

        [HttpGet]
        [Route(ApiConstants.ApiRoutes.AccountRoutes.GetAccounts)]
        public async Task<IActionResult> GetAccounts([FromQuery]string search,[FromQuery]QueryPagingParameters parameters)
        {
            var response = await _accountService.GetAccounts(search, parameters.PageSize.GetValueOrDefault(), parameters.Page.GetValueOrDefault());
            return response?.Value;
        }
    }
}
