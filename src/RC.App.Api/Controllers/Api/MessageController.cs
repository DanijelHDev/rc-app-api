﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using static RC.Bll.Common.Helpers.Constants;
using RC.Bll.Api.Models.Request;
using RC.Bll.Api.Models.Validation;
using System.Net;
using RC.Bll.Api.Models.HttpResult;
using RC.Bll.Api.Services.Messages;

namespace RC.App.Api.Controllers.Api
{
    [Authorize]
    [Route(ApiConstants.ApiRoutes.Base.BaseRoute)]
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        [Route(ApiConstants.ApiRoutes.MessageRoutes.GetMessage)]
        public async Task<IActionResult> GetMessage([FromQuery] Guid? withAccountId, [FromQuery]QueryPagingParameters parameters)
        {
            var response = await _messageService.GetAll(withAccountId, parameters.PageSize.GetValueOrDefault(), parameters.Page.GetValueOrDefault());
            return response?.Value;
        }

        [HttpGet]
        [Route(ApiConstants.ApiRoutes.MessageRoutes.GetUnreadCount)]
        public async Task<IActionResult> GetUnreadCountOfMessage([FromQuery]QueryPagingParameters parameters)
        {
            var response = await _messageService.GetUnreadCount();
            return response?.Value;
        }

        [HttpPost]
        [Route(ApiConstants.ApiRoutes.MessageRoutes.SendMessage)]
        public async Task<IActionResult> SendMessage([FromBody]MessageRequest request)
        {
            var validator = new MessageRequestValidation();
            var results = validator.Validate(request);
            if (!results.IsValid)
            {
                return new RcHttpResult(
                    HttpStatusCode.BadRequest,
                    ErrorHandler.CreateErrorResponse(results.Errors.Select(e => e.ErrorMessage).ToList()))
                    .Value;
            }
            
            var response = await _messageService.Send(request);

            if (response.Value.StatusCode != (int)HttpStatusCode.OK)
                return response.Value;

            return Ok();
        }

        [HttpDelete]
        [Route(ApiConstants.ApiRoutes.MessageRoutes.DeleteMessage)]
        public async Task<IActionResult> DeleteMessages([FromBody]List<Guid> request)
        {
            var response = await _messageService.Delete(request);
            
            return response.Value;
        }

        [HttpPut]
        [Route(ApiConstants.ApiRoutes.MessageRoutes.UpdateMessage)]
        public async Task<IActionResult> UpdateMessages([FromBody]List<Guid> request)
        {
            var response = await _messageService.FlagIsRead(request);

            return response.Value;
        }
    }
}
