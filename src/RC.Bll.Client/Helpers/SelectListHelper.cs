﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RC.Bll.Client.Helpers
{
    public static class SelectListHelper
    {
        /// <summary>
        /// Dropdown helper
        /// </summary>
        public static IEnumerable<SelectListItem> FillDropdownValues<TEntity>(IList<TEntity> entities, Func<TEntity, object> funcToGetValue, Func<TEntity, object> funcToGetText)
        {
            return entities.OrderBy(x => funcToGetText(x).ToString())
                    .Select(x => new SelectListItem
                    {
                        Value = funcToGetValue(x).ToString(),
                        Text = funcToGetText(x).ToString()
                    });
        }

        /// <summary>
        /// Dropdown helper. Creates SelectList.
        /// </summary>
        public static SelectList FillDropdownValues<TEntity>(IList<TEntity> entities, string value, string text, string selectedId)
        {
            return new SelectList(entities, value, text, selectedValue: selectedId);
        }

        /// <summary>
        /// Dropdown helper. Creates SelectList.
        /// </summary>
        public static SelectList FillDropdownValues<TEntity>(IList<TEntity> entities, string value, string text, Guid selectedId)
        {
            return new SelectList(entities, value, text, selectedValue: selectedId);
        }

        /// <summary>
        /// Dropdown helper. Creates MultiSelectList.
        /// </summary>
        public static MultiSelectList FillDropdownValues<TEntity>(IList<TEntity> entities, string value, string text, Guid[] selectedIds)
        {
            return new MultiSelectList(entities, value, text, selectedValues: selectedIds);
        }

        /// <summary>
        /// Dropdown helper. Creates MultiSelectList.
        /// </summary>
        public static MultiSelectList FillDropdownValues<TEntity>(IList<TEntity> entities, string value, string text, List<Guid> selectedIds)
        {
            return new MultiSelectList(entities, value, text, selectedValues: selectedIds);
        }
    }
}
