﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Client.Helpers
{
    public static class RcHttpClient
    {
        private static HttpClient _client;

        public static HttpClient Instance
        {
            get
            {
                if (_client == null)
                {
                    _client = new HttpClient();
                    _client.BaseAddress = new Uri(WebConstants.ApiRoutes.BaseRoute);
                    _client.DefaultRequestHeaders.Clear();
                }

                return _client;
            }
        }

        public static HttpClient InstanceWithAuth(string accessToken)
        {
            if (_client == null)
            {
                _client = new HttpClient();
                _client.BaseAddress = new Uri(WebConstants.ApiRoutes.BaseRoute);
            }

            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            return _client;
        }
    }
}
