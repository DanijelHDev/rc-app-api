﻿using RC.Bll.Client.Services.Authentication;
using Microsoft.Extensions.DependencyInjection;
using RC.Bll.Client.Services.Accounts;
using RC.Bll.Client.Services.Messages;

namespace RC.Bll.Client.Helpers
{
    public static class DiModules
    {
        public static IServiceCollection RegisterWebServices(this IServiceCollection services)
        {
            services.AddTransient<ICurrentAccount, CurrentAccount>();

            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IRcAuthentication, RcAuthentication>();
            
            return services;
        }
    }
}
