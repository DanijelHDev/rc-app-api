﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.ViewModels
{
    public class TokenVm
    {
        public string AccessToken { get; set; }
        public string Username { get; set; }
        public int ExpiresIn { get; set; }
        public Guid Id { get; set; }
        public string FullName { get; set; }
    }
}
