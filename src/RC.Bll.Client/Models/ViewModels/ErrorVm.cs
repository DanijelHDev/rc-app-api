﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.ViewModels
{
    public class ErrorVm
    {
        public List<ErrorMessage> Errors { get; set; }
    }

    public class ErrorMessage
    {
        public string Error { get; set; }
    }
}
