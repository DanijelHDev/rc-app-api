﻿
namespace RC.Bll.Client.Models.ViewModels
{
    public class DropdownSelectItem<T>
    {
        public T Value { get; set; }
        public string Display { get; set; }
        public bool Selected { get; set; } = false;
    }
}
