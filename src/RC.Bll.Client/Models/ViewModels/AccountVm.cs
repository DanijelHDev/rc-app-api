﻿using RC.Bll.Client.Models.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.ViewModels
{
    public class AccountsVm
    {
        public List<AccountVm> Accounts = new List<AccountVm>();
        public AccountFilter Filter { get; set; }
    }

    public class AccountVm
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
