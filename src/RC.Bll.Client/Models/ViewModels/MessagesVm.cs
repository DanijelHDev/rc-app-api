﻿using RC.Bll.Client.Models.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.ViewModels
{
    public class MessagesVm
    {
        public List<MessageVm> Messages = new List<MessageVm>();
        public MessageFilter Filter = new MessageFilter();
    }

    public class MessageVm
    {
        public Guid Id { get; set; }
        
        [Required]
        public string Text { get; set; }
        public Guid FromAccountId { get; set; }
        public string FromFullName { get; set; }
        public Guid ToAccountId { get; set; }
        public string ToFullName { get; set; }
        public DateTime SentOn { get; set; }
        public bool IsRead { get; set; }
    }
}
