﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RC.Bll.Client.Models.Filter
{
    public class MessageFilter : BaseFilter
    {
        public Guid WithAccountId { get; set; }
        public SelectList AccountsDropdown { get; set; }
    }
}
