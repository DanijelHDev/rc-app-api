﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.Filter
{
    public class AccountFilter : BaseFilter
    {
        public string Search { get; set; }
    }
}
