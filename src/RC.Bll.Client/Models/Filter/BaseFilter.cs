﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Client.Models.Filter
{
    public abstract class BaseFilter
    {
        public int Page { get; set; } = 0;
        public int PageSize { get; set; } = 10;
        public bool IsLastPage { get; set; } = false;
        public bool IsFilterClicked { get; set; } = false;

        public int ToSkip
        {
            get
            {
                return Page * PageSize;
            }
        }
    }
}
