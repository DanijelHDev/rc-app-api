﻿using RC.Bll.Client.Helpers;
using RC.Bll.Client.Models.Authentication;
using RC.Bll.Client.Models.Filter;
using RC.Bll.Client.Models.ViewModels;
using RC.Bll.Client.Services.Authentication;
using RC.Bll.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Client.Services.Accounts
{
    public class AccountService : IAccountService
    {
        private readonly ICurrentAccount _currentAccount;

        public AccountService(ICurrentAccount currentAccount)
        {
            _currentAccount = currentAccount;
        }

        public async Task<AuthenticationResult> RegisterNew(RegisterVm model)
        {
            var response = await RcHttpClient.Instance.PostAsync(WebConstants.ApiRoutes.Register, new StringContent(model.ToJson(), Encoding.UTF8, "application/json"));

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var responseString = await response.Content.ReadAsStringAsync();

                var responseArray = responseString.Deserialize<ErrorVm>()?.Errors.Select(r => r.Error).ToArray();
                var errors = String.Join(" | ", responseArray);

                return new AuthenticationResult(String.Join(String.Empty, errors));
            }
                
            return new AuthenticationResult(String.Empty);
        }

        public async Task<AccountsVm> GetAll(AccountFilter filter)
        {
            var accountsVm = new AccountsVm
            {
                Filter = filter
            };

            string queryParameters = String.Empty;
            queryParameters = $"?{nameof(filter.Page)}={filter.Page}";
            queryParameters = $"{queryParameters}&{nameof(filter.PageSize)}={filter.PageSize}";

            if (!String.IsNullOrEmpty(filter.Search))
                queryParameters = $"{queryParameters}&{nameof(filter.Search)}={filter.Search}";

            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).GetAsync($"{WebConstants.ApiRoutes.AccountList}{queryParameters}");
            
            var stringResponse = await response.Content.ReadAsStringAsync();
            accountsVm.Accounts = stringResponse.Deserialize<List<AccountVm>>();

            if (accountsVm?.Accounts?.Count < filter.PageSize)
                accountsVm.Filter.IsLastPage = true;

            accountsVm.Filter.Page += 1;

            return accountsVm;
        }
    }
}
