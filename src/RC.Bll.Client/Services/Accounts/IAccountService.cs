﻿using RC.Bll.Client.Models.Authentication;
using RC.Bll.Client.Models.Filter;
using RC.Bll.Client.Models.ViewModels;
using System.Threading.Tasks;

namespace RC.Bll.Client.Services.Accounts
{
    public interface IAccountService
    {
        /// <summary>
        /// Register new RC account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<AuthenticationResult> RegisterNew(RegisterVm model);

        /// <summary>
        /// Get account list
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<AccountsVm> GetAll(AccountFilter filter);
    }
}
