﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace RC.Bll.Client.Services.Authentication
{
    public interface ICurrentAccount
    {
        Guid Id { get; }
        string AccessToken { get; }
        string Username { get; }
        string FullName { get; }
    }
}