﻿using RC.Bll.Client.Models.Authentication;
using System.Threading.Tasks;

namespace RC.Bll.Client.Services.Authentication
{
    public interface IRcAuthentication
    {
        Task SignOut();

        Task<AuthenticationResult> SignIn(string username, string password, bool rememberMe);
    }
}
