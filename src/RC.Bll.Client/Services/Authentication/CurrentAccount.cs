﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Client.Services.Authentication
{
    public class CurrentAccount : ICurrentAccount
    {
        private readonly IHttpContextAccessor _accessor;

        public CurrentAccount(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public Guid Id
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return Guid.Empty;

                var id = _accessor.HttpContext.User.Claims.Where(c => c.Type == WebConstants.AccountClaims.Id)
                    .Select(c => c.Value).FirstOrDefault();

                if (String.IsNullOrEmpty(id))
                    return Guid.Empty;

                Guid accountId;
                if (!Guid.TryParse(id, out accountId))
                    return Guid.Empty;

                return accountId;
            }
        }

        public string Username
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return "System";

                return _accessor.HttpContext.User.Identity.Name;
            }
        }

        public string AccessToken
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return String.Empty;

                var accessToken = _accessor.HttpContext.User.Claims.Where(c => c.Type == WebConstants.AccountClaims.AccessToken)
                    .Select(c => c.Value).FirstOrDefault();

                return accessToken;
            }
        }

        public string FullName
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return String.Empty;

                var fullName = _accessor.HttpContext.User.Claims.Where(c => c.Type == WebConstants.AccountClaims.FullName)
                    .Select(c => c.Value).FirstOrDefault();
                
                return fullName;
            }
        }
    }
}