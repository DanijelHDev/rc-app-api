﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Newtonsoft.Json;
using RC.Bll.Client.Helpers;
using RC.Bll.Client.Models.Authentication;
using RC.Bll.Client.Models.ViewModels;
using RC.Bll.Common.Helpers;
using System;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Client.Services.Authentication
{
    public class RcAuthentication : IRcAuthentication
    {
        private bool _isAuthenticated;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly AuthenticationManager _authenticationManager;

        public RcAuthentication(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
            _authenticationManager = _contextAccessor.HttpContext.Authentication;
            _isAuthenticated = false;
        }

        public async Task SignOut()
        {
            await _authenticationManager.SignOutAsync(WebConstants.Authorization.CookieName);
        }

        public async Task<AuthenticationResult> SignIn(string username, string password, bool rememberMe)
        {
            var data = new
            {
                username = username,
                password = password
            }.ToJson();
            
            var response = await RcHttpClient.Instance.PostAsync(WebConstants.ApiRoutes.VerifyAccount, new StringContent(data, Encoding.UTF8, "application/json"));
            
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                return new AuthenticationResult("Username or Password is not correct.");

            var stringResponse = await response.Content.ReadAsStringAsync();
            var accountData = JsonConvert.DeserializeObject<TokenVm>(stringResponse);

            var claimsIdentity = CreateIdentity(accountData);
            var accountIdentity = new ClaimsPrincipal(claimsIdentity);

            await _authenticationManager.SignOutAsync(WebConstants.Authorization.CookieName);
            if (rememberMe)
            {
                await _authenticationManager.SignInAsync(WebConstants.Authorization.CookieName, accountIdentity,
                new AuthenticationProperties
                {
                    IsPersistent = true
                });
            }
            else
            {
                await _authenticationManager.SignInAsync(WebConstants.Authorization.CookieName, accountIdentity,
                new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(120)
                });
            }

            return new AuthenticationResult(String.Empty);
        }

        private ClaimsIdentity CreateIdentity(TokenVm accountData)
        {
            var identity = new ClaimsIdentity(
                WebConstants.Authorization.CookieName,
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "OAuth"));
            identity.AddClaim(new Claim(WebConstants.AccountClaims.Id, accountData?.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Name, accountData?.Username));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, accountData?.Username));
            identity.AddClaim(new Claim(WebConstants.AccountClaims.FullName, accountData?.FullName));
            identity.AddClaim(new Claim(WebConstants.AccountClaims.AccessToken, accountData?.AccessToken));
            
            return identity;
        }
    }
}
