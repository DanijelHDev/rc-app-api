﻿using RC.Bll.Client.Models.Filter;
using RC.Bll.Client.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RC.Bll.Client.Services.Messages
{
    public interface IMessageService
    {
        /// <summary>
        /// Get all messages
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<MessagesVm> GetAll(MessageFilter filter);

        /// <summary>
        /// Send new message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> Send(MessageVm model);

        /// <summary>
        /// Get number of unread messages
        /// </summary>
        /// <returns></returns>
        Task<int> GetUnreadMessageCount();

        /// <summary>
        /// Delete messages
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<bool> DeleteMessages(List<Guid> ids);

        /// <summary>
        /// Populates form data for filter
        /// </summary>
        /// <returns></returns>
        Task<MessageFilter> InitializeFilterFormValues();
    }
}
