﻿using RC.Bll.Client.Helpers;
using RC.Bll.Client.Models.Filter;
using RC.Bll.Client.Models.ViewModels;
using RC.Bll.Client.Services.Accounts;
using RC.Bll.Client.Services.Authentication;
using RC.Bll.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Client.Services.Messages
{
    public class MessageService : IMessageService
    {
        private readonly IAccountService _accountService;
        private readonly ICurrentAccount _currentAccount;

        public MessageService(IAccountService accountService, ICurrentAccount currentAccount)
        {
            _accountService = accountService;
            _currentAccount = currentAccount;
        }

        public async Task<MessagesVm> GetAll(MessageFilter filter)
        {
            var messagesVm = new MessagesVm
            {
                Filter = filter
            };

            string queryParameters = String.Empty;
            queryParameters = $"?{nameof(filter.Page)}={filter.Page}";
            queryParameters = $"{queryParameters}&{nameof(filter.PageSize)}={filter.PageSize}";

            if (filter.WithAccountId.IsValidGuid())
                queryParameters = $"{queryParameters}&{nameof(filter.WithAccountId)}={filter.WithAccountId}";

            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).GetAsync($"{WebConstants.ApiRoutes.GetMessages}{queryParameters}");

            var stringResponse = await response.Content.ReadAsStringAsync();
            messagesVm.Messages = stringResponse.Deserialize<List<MessageVm>>();

            if (messagesVm?.Messages?.Count < filter.PageSize)
                messagesVm.Filter.IsLastPage = true;

            messagesVm.Filter.Page += 1;

            // Update unread messages status
            await UpdateMessagesAsRead(messagesVm.Messages.Where(m => !m.IsRead).Select(m => m.Id).ToList());

            return messagesVm;
        }

        public async Task<bool> Send(MessageVm model)
        {
            var data = new
            {
                ToAccountId = model.ToAccountId,
                Text = model.Text
            };

            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).PostAsync(WebConstants.ApiRoutes.SendMessage, new StringContent(data.ToJson(), Encoding.UTF8, "application/json"));
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                return await Task.FromResult<bool>(false);

            return await Task.FromResult<bool>(true);
        }

        public async Task<int> GetUnreadMessageCount()
        {
            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).GetAsync($"{WebConstants.ApiRoutes.GetUnreadMessageCount}");

            var stringResponse = await response.Content.ReadAsStringAsync();

            var count = stringResponse.Deserialize<List<int>>();

            return count[0];
        }

        public async Task<bool> DeleteMessages(List<Guid> ids)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{WebConstants.ApiRoutes.BaseRoute}/{WebConstants.ApiRoutes.DeleteMessages}"),
                Content = new StringContent(ids.ToJson(), Encoding.UTF8, "application/json")
            };

            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            if(response.StatusCode != System.Net.HttpStatusCode.NoContent)
                await Task.FromResult<bool>(false);

            return await Task.FromResult<bool>(true);
        }

        private async Task UpdateMessagesAsRead(List<Guid> ids)
        {
            if(!ids.Any())
                return;

            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Put,
                RequestUri = new Uri($"{WebConstants.ApiRoutes.BaseRoute}/{WebConstants.ApiRoutes.UpdateMessages}"),
                Content = new StringContent(ids.ToJson(), Encoding.UTF8, "application/json")
            };

            var response = await RcHttpClient.InstanceWithAuth(_currentAccount.AccessToken).SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                await Task.FromResult<bool>(false);

            return;
        }

        public async Task<MessageFilter> InitializeFilterFormValues()
        {
            // Not really smart thing for page size, but for demo app purpose will do :)
            var accountVm = await _accountService.GetAll(new AccountFilter { Page = 0, PageSize = Int16.MaxValue });
            if (accountVm == null || !accountVm.Accounts.Any())
                return new MessageFilter();

            var dropdownData = accountVm.Accounts
                    .Select(o => new DropdownSelectItem<Guid>
                    {
                        Value = o.Id,
                        Display = $"{o.FirstName} {o.LastName}"
                    }).ToList();

            // Remove self
            dropdownData = dropdownData.Where(d => d.Display != _currentAccount.FullName).ToList();

            var organizationDropdown = SelectListHelper.FillDropdownValues(dropdownData,
                StaticReflection.GetMemberName<DropdownSelectItem<Guid>>(x => x.Value),
                StaticReflection.GetMemberName<DropdownSelectItem<Guid>>(x => x.Display), "");

            return new MessageFilter
            {
                AccountsDropdown = organizationDropdown
            };
        }
    }
}
