﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace RC.Bll.Common.Helpers
{
    public static class Constants
    {
        public static class WebConstants
        {
            public static class Authorization
            {
                public const string CookieName = "RcAuthMiddlewareInstance";
            }

            public static class AccountClaims
            {
                public const string Id = "Id";
                public const string Username = "Username";
                public const string AccessToken = "AccessToken";
                public const string FullName = "DisplayName";
            }

            public static class ApiRoutes
            {
                public const string BaseRoute = "http://localhost:1121";

                public const string VerifyAccount = "api/token";
                public const string Register = "api/accounts";
                public const string AccountList = "api/accounts";
                public const string GetMessages = "api/messages";
                public const string SendMessage = "api/messages";
                public const string DeleteMessages = "api/messages";
                public const string UpdateMessages = "api/messages";
                public const string GetUnreadMessageCount = "api/messages/unread";
            }
        
            
        }

        public static class ApiConstants
        {
            public static class ApiSettings
            {
                public const string SecretKey = "rcTopSecretSuperLongKeyForOAuth";
                public static readonly SymmetricSecurityKey SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

                public static readonly SigningCredentials SigningCredentials = new SigningCredentials(SigningKey, SecurityAlgorithms.HmacSha256);

                public const string Issuer = "RcTokenServer";
                public const string Audience = "http://localhost:1121/";
            }

            public static class ApiRoutes
            {
                public static class Base
                {
                    public const string BaseRoute = "api";
                }

                public static class AccountRoutes
                {
                    public const string Register = "accounts";
                    public const string GetAccounts = "accounts";
                }

                public static class MessageRoutes
                {
                    public const string GetMessage = "messages";
                    public const string SendMessage = "messages";
                    public const string DeleteMessage = "messages";
                    public const string UpdateMessage = "messages";
                    public const string GetUnreadCount = "messages/unread";
                }

                public static class TokenRoutes
                {
                    public const string TokenEndpoint = "token";
                }
            }
        }
    }
}
