﻿using Newtonsoft.Json;
using System;

namespace RC.Bll.Common.Helpers
{
    public static class Extensions
    {
        public static bool IsValidGuid(this Guid uuid)
        {
            if (uuid == Guid.Empty)
                return false;

            return true;
        }

        public static bool IsValidGuid(this Guid? uuid)
        {
            if (uuid == null || uuid == Guid.Empty)
                return false;

            return true;
        }

        public static string ToJson(this object obj)
        {
            try
            {
                var json = JsonConvert.SerializeObject(obj);
                return json;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static T Deserialize<T>(this string objAsString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(objAsString);
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
