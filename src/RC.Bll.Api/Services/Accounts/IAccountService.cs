﻿using RC.Bll.Api.Models.HttpResult;
using RC.Bll.Api.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Api.Services.Accounts
{
    public interface IAccountService
    {
        /// <summary>
        /// Get list of accounts
        /// </summary>
        /// <param name="searchParam"></param>
        /// <returns></returns>
        Task<RcHttpResult> GetAccounts(string searchParam, int pageSize, int page);

        /// <summary>
        /// Register new RC account
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<RcHttpResult> Register(RegisterRequest request);
    }
}
