﻿using RC.Bll.Api.Helpers.Authentication;
using RC.Bll.Api.Models.Request;
using RC.Core.DataAccess.UnitOfWork;
using RC.Core.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using RC.Bll.Api.Models.Response;
using RC.Core.Entity.Common;
using RC.Bll.Api.Models.HttpResult;
using System.Data;

namespace RC.Bll.Api.Services.Accounts
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<RcHttpResult> GetAccounts(string searchParam, int pageSize, int page)
        {
            SqlParameter SearchParam = new SqlParameter
            {
                ParameterName = $"@{nameof(SearchParam)}",
                Value = (object)searchParam ?? (object)DBNull.Value,
                DbType = DbType.String
            };
            SqlParameter Skip = new SqlParameter($"@{nameof(Skip)}", page * pageSize);
            SqlParameter Take = new SqlParameter($"@{nameof(Take)}", pageSize);

            var accountData = await _unitOfWork.SqlQuery<AccountSpResponse>(
                $"{DbStoredProcedures.AccountListData} @{nameof(SearchParam)}, @{nameof(Skip)}, @{nameof(Take)}",
                SearchParam, Skip, Take).ToListAsync();

            #region EF way
            //var query = _unitOfWork.Accounts.GetAllQueryableNoTracking();

            //if (!String.IsNullOrEmpty(searchParam))
            //    query = query.Where(a => a.Username.Contains(searchParam)
            //            || a.FirstName.Contains(searchParam)
            //            || a.LastName.Contains(searchParam));

            //var accountData = await query    
            //    .Select(a => new AccountSpResponse
            //    {
            //        Id = a.Id,
            //        Username = a.Username,
            //        FirstName = a.FirstName,
            //        LastName = a.LastName
            //    }).ToListAsync();
            #endregion

            accountData.ForEach(a => a.Password = null);
            return new RcHttpResult(System.Net.HttpStatusCode.OK, accountData);
        }

        public async Task<RcHttpResult> Register(RegisterRequest request)
        {
            SqlParameter Username = new SqlParameter($"@{nameof(Username)}", request.Username);

            var accountData = await _unitOfWork.SqlQuery<AccountSpResponse>(
                $"{DbStoredProcedures.AccountData} @{nameof(Username)}",
                Username).FirstOrDefaultAsync();

            if (accountData != null)
                return new RcHttpResult(System.Net.HttpStatusCode.Conflict, ErrorHandler.CreateErrorResponse("User with that username already exists"));

            var account = new Account
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Username = request.Username,
                Password = PasswordManager.HashPassword(request.Password)
            };
            
            _unitOfWork.Accounts.SetEntityState(account, EntityState.Added);
            await _unitOfWork.ExecuteUpdatesAsync();

            return new RcHttpResult(System.Net.HttpStatusCode.OK);
        }
    }
}
