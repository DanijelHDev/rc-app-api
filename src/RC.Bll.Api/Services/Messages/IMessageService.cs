﻿using RC.Bll.Api.Models.HttpResult;
using RC.Bll.Api.Models.Request;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RC.Bll.Api.Services.Messages
{
    public interface IMessageService
    {
        /// <summary>
        /// Send new message. Each account get's it's own copy
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<RcHttpResult> Send(MessageRequest request);

        /// <summary>
        /// Get all messages for current account
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        Task<RcHttpResult> GetAll(Guid? withAccountId, int pageSize, int page);

        /// <summary>
        /// Flag messages as read
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<RcHttpResult> FlagIsRead(List<Guid> ids);

        /// <summary>
        /// Flag messages as deleted
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<RcHttpResult> Delete(List<Guid> ids);

        /// <summary>
        /// Get number of unread messages for current user
        /// </summary>
        /// <returns></returns>
        Task<RcHttpResult> GetUnreadCount();
    }
}
