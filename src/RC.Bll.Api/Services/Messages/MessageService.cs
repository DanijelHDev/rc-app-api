﻿using RC.Bll.Api.Models.HttpResult;
using RC.Bll.Api.Models.Request;
using RC.Bll.Api.Models.Response;
using RC.Bll.Api.Services.Authentication;
using RC.Core.DataAccess.UnitOfWork;
using RC.Core.Entity.Common;
using RC.Core.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RC.Bll.Api.Services.Messages
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentAccount _currentAccount;

        public MessageService(IUnitOfWork unitOfWork, ICurrentAccount currentAccount)
        {
            _unitOfWork = unitOfWork;
            _currentAccount = currentAccount;
        }

        public async Task<RcHttpResult> Send(MessageRequest request)
        {
            var senderMessage = CreateMessage(request, isSenderMessage: true);
            var receieverMessage = CreateMessage(request);

            _unitOfWork.Messages.SetEntityState(senderMessage, EntityState.Added);

            if(request.ToAccountId != _currentAccount.Id)
                _unitOfWork.Messages.SetEntityState(receieverMessage, EntityState.Added);

            await _unitOfWork.ExecuteUpdatesAsync(_currentAccount.Username);

            return new RcHttpResult(System.Net.HttpStatusCode.OK);
        }

        public async Task<RcHttpResult> GetUnreadCount()
        {
            SqlParameter OwnerId = new SqlParameter
            {
                ParameterName = $"@{nameof(OwnerId)}",
                Value = (object)_currentAccount.Id ?? (object)DBNull.Value,
                DbType = DbType.Guid
            };

            var unreadCount = await _unitOfWork.SqlQuery<int>(
                $"{DbStoredProcedures.MessagesUnread} @{nameof(OwnerId)}",
                OwnerId).ToListAsync();

            return new RcHttpResult(System.Net.HttpStatusCode.OK, unreadCount);
        }

        public async Task<RcHttpResult> GetAll(Guid? withAccountId, int pageSize, int page)
        {
            SqlParameter OwnerId = new SqlParameter
            {
                ParameterName = $"@{nameof(OwnerId)}",
                Value = (object)_currentAccount.Id ?? (object)DBNull.Value,
                DbType = DbType.Guid
            };
            SqlParameter WithAccountId = new SqlParameter
            {
                ParameterName = $"@{nameof(WithAccountId)}",
                Value = (object)withAccountId ?? (object)DBNull.Value,
                DbType = DbType.Guid
            };
            SqlParameter Skip = new SqlParameter($"@{nameof(Skip)}", page * pageSize);
            SqlParameter Take = new SqlParameter($"@{nameof(Take)}", pageSize);

            var messagesData = await _unitOfWork.SqlQuery<MessagesSpResponse>(
                $"{DbStoredProcedures.MessagesForAccount} @{nameof(OwnerId)}, @{nameof(WithAccountId)}, @{nameof(Skip)}, @{nameof(Take)}",
                OwnerId, WithAccountId, Skip, Take).ToListAsync();

            return new RcHttpResult(System.Net.HttpStatusCode.OK, messagesData);
        }

        public async Task<RcHttpResult> FlagIsRead(List<Guid> ids)
        {
            DataTable dataTable = new DataTable("IdList");
            dataTable.Columns.Add("Id", typeof(Guid));
            ids.ForEach(id => dataTable.Rows.Add(id));

            SqlParameter IdList = new SqlParameter
            {
                ParameterName = $"@{nameof(IdList)}",
                Value = dataTable,
                SqlDbType = SqlDbType.Structured,
                TypeName = "dbo.GuidList"
            };
            SqlParameter OwnerId = new SqlParameter
            {
                ParameterName = $"@{nameof(OwnerId)}",
                Value = (object)_currentAccount.Id ?? (object)DBNull.Value,
                DbType = DbType.Guid
            };
            SqlParameter Username = new SqlParameter($"@{nameof(Username)}", _currentAccount.Username);

            await _unitOfWork.SqlCommand($"{DbStoredProcedures.MessagesFlagRead} @{nameof(IdList)}, @{nameof(OwnerId)}, @{nameof(Username)}",
                IdList, OwnerId, Username);

            return new RcHttpResult(System.Net.HttpStatusCode.OK);
        }

        public async Task<RcHttpResult> Delete(List<Guid> ids)
        {
            DataTable dataTable = new DataTable("IdList");
            dataTable.Columns.Add("Id", typeof(Guid));
            ids.ForEach(id => dataTable.Rows.Add(id));

            SqlParameter IdList = new SqlParameter
            {
                ParameterName = $"@{nameof(IdList)}",
                Value = dataTable,
                SqlDbType = SqlDbType.Structured,
                TypeName = "dbo.GuidList"
            };
            SqlParameter OwnerId = new SqlParameter
            {
                ParameterName = $"@{nameof(OwnerId)}",
                Value = (object)_currentAccount.Id ?? (object)DBNull.Value,
                DbType = DbType.Guid
            };
            SqlParameter Username = new SqlParameter($"@{nameof(Username)}", _currentAccount.Username);

            await _unitOfWork.SqlCommand($"{DbStoredProcedures.MessagesFlagDeleted} @{nameof(IdList)}, @{nameof(OwnerId)}, @{nameof(Username)}",
                IdList, OwnerId, Username);
            
            return new RcHttpResult(System.Net.HttpStatusCode.OK);
        }
        
        private Message CreateMessage(MessageRequest request, bool isSenderMessage = false)
        {
            var message = new Message
            {
                Id = Guid.NewGuid(),
                Text = request.Text,
                FromAccountId = _currentAccount.Id,
                ToAccountId = request.ToAccountId,
                OwnerId = request.ToAccountId
            };

            if (isSenderMessage)
            {
                message.IsRead = true;
                message.OwnerId = _currentAccount.Id;
            }

            return message;
        }
    }
}
