﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace RC.Bll.Api.Services.Authentication
{
    public class CurrentAccount : ICurrentAccount
    {
        private readonly IHttpContextAccessor _accessor;

        public CurrentAccount(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public Guid Id
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return Guid.Empty;

                var id = _accessor.HttpContext.User.Claims.Where(c => c.Type == "Id")
                    .Select(c => c.Value).FirstOrDefault();

                if (String.IsNullOrEmpty(id))
                    return Guid.Empty;

                Guid accountId;
                if (!Guid.TryParse(id, out accountId))
                    return Guid.Empty;

                return accountId;
            }
        }

        public string Username
        {
            get
            {
                if (String.IsNullOrEmpty(_accessor.HttpContext.User.Identity.Name))
                    return "System";

                return _accessor.HttpContext.User.Identity.Name;
            }
        }
    }
}