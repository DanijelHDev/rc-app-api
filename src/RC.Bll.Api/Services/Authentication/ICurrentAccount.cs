﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace RC.Bll.Api.Services.Authentication
{
    public interface ICurrentAccount
    {
        Guid Id { get; }
        
        string Username { get; }
    }
}