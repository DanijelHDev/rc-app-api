﻿using RC.Bll.Api.Helpers.Authentication;
using RC.Bll.Api.Models.Response;
using RC.Core.DataAccess.UnitOfWork;
using RC.Core.Entity.Common;
using RC.Core.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Api.Services.Authentication
{
    public class RcAuthentication : IRcAuthentication
    {
        private readonly IUnitOfWork _unitOfWork;

        public RcAuthentication(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public async Task<ClaimsIdentity> GetAccountIdentity(string username, string password)
        {
            //Check if user exists in database
            SqlParameter Username = new SqlParameter($"@{nameof(Username)}", username);

            var accountData = await _unitOfWork.SqlQuery<AccountSpResponse>(
                $"{DbStoredProcedures.AccountData} @{nameof(Username)}",
                Username).FirstOrDefaultAsync();
            
            if (accountData == null || !PasswordManager.VerifyHashedPassword(accountData.Password, password))
                return null;
            
            return CreateIdentity(accountData);
        }

        private ClaimsIdentity CreateIdentity(AccountSpResponse account)
        {
            var identity = new ClaimsIdentity(
                WebConstants.Authorization.CookieName,
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "RcOAuth"));
            identity.AddClaim(new Claim(WebConstants.AccountClaims.Id, account.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Name, account.Username));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, account.Username));
            identity.AddClaim(new Claim(WebConstants.AccountClaims.FullName, $"{account.FirstName} {account.LastName}"));
            
            return identity;
        }
    }
}
