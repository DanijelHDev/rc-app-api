﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RC.Bll.Api.Services.Authentication
{
    public interface IRcAuthentication
    {
        /// <summary>
        /// Get account identity data
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ClaimsIdentity> GetAccountIdentity(string username, string password);
    }
}
