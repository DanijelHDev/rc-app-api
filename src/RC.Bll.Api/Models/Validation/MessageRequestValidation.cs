﻿using FluentValidation;
using RC.Bll.Api.Models.Request;
using RC.Bll.Common.Helpers;
using System;

namespace RC.Bll.Api.Models.Validation
{
    public class MessageRequestValidation : AbstractValidator<MessageRequest>
    {
        public MessageRequestValidation()
        {
            RuleFor(m => m.Text)
                .NotNull()
                .Length(1, Int16.MaxValue)
                .WithMessage("Text is required");

            RuleFor(m => m.ToAccountId)
                .Must(BeAValidGuid)
                .WithMessage("Account id is not valid.");
        }

        private bool BeAValidGuid(Guid accountId)
        {
            return accountId.IsValidGuid();
        }
    }
}
