﻿using FluentValidation;
using RC.Bll.Api.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Api.Models.Validation
{
    public class RegisterRequestValidation : AbstractValidator<RegisterRequest>
    {
        public RegisterRequestValidation()
        {
            RuleFor(m => m.FirstName)
                .NotNull()
                .Length(1, Int16.MaxValue)
                .WithMessage("First name is required");

            RuleFor(m => m.LastName)
                .NotNull()
                .Length(1, Int16.MaxValue)
                .WithMessage("Last name is required");

            RuleFor(m => m.Username)
                .NotNull()
                .Length(1, Int16.MaxValue)
                .WithMessage("Username is required");

            RuleFor(m => m.Password)
                .NotNull()
                .Length(1, Int16.MaxValue)
                .WithMessage("Password is required");
        }
    }
}
