﻿using System;

namespace RC.Bll.Api.Models.Response
{
    public class MessagesSpResponse
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public Guid FromAccountId { get; set; }
        public string FromFullName { get; set; }
        public Guid ToAccountId { get; set; }
        public string ToFullName { get; set; }
        public DateTime SentOn { get; set; }
        public bool IsRead { get; set; }
    }
}
