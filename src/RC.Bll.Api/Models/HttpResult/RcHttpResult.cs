﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RC.Bll.Api.Models.HttpResult
{
    public class RcHttpResult
    {
        private readonly ObjectResult _objectResult;

        public RcHttpResult(HttpStatusCode statusCode, object content)
        {
            this._objectResult = new ObjectResult(content);
            this._objectResult.StatusCode = (int)statusCode;
        }

        public RcHttpResult(HttpStatusCode statusCode)
        {
            this._objectResult = new ObjectResult(null);
            this._objectResult.StatusCode = (int)statusCode;
        }

        public ObjectResult Value
        {
            get
            {
                return _objectResult;
            }
        }
    }

    public static class ErrorHandler
    {
        public static ErrorResponse CreateErrorResponse(string message)
        {
            return new ErrorResponse
            {
                Errors = new List<ErrorMessage>
                {
                    new ErrorMessage
                    {
                        Error = message
                    }
                }
            };
        }

        public static ErrorResponse CreateErrorResponse(List<string> messages)
        {
            List<ErrorMessage> errors = new List<ErrorMessage>();
            messages.ForEach(msg => errors.Add(new ErrorMessage { Error = msg }));

            return new ErrorResponse
            {
                Errors = errors
            };
        }
    }

    public class ErrorResponse
    {
        public List<ErrorMessage> Errors { get; set; }
    }

    public class ErrorMessage
    {
        public string Error { get; set; }
    }
}
