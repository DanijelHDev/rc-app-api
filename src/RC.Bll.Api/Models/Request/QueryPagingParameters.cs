﻿
namespace RC.Bll.Api.Models.Request
{
    public class QueryPagingParameters
    {
        private int? _page;
        public int? Page
        {
            get
            {
                if (_pageSize != null && _page == null)
                    _page = 0;

                return _page;
            }
            set
            {
                _page = value;
            }
        }
        private int? _pageSize;
        public int? PageSize
        {
            get
            {
                if (_page != null && _pageSize == null)
                    _pageSize = 10;

                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }
    }
}
