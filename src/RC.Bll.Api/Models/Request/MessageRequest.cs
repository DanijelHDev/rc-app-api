﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RC.Bll.Api.Models.Request
{
    public class MessageRequest
    {
        public Guid ToAccountId { get; set; }
        public string Text { get; set; }
    }
}
