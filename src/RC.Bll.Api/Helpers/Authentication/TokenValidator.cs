﻿using Microsoft.IdentityModel.Tokens;
using System;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.Bll.Api.Helpers.Authentication
{
    public static class TokenValidator
    {
        public static TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = ApiConstants.ApiSettings.Issuer,

                ValidateAudience = true,
                ValidAudience = ApiConstants.ApiSettings.Audience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = ApiConstants.ApiSettings.SigningKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };
        }
    }
}
