﻿using Microsoft.Extensions.DependencyInjection;
using RC.Bll.Api.Services.Accounts;
using RC.Bll.Api.Services.Authentication;
using RC.Core.DataAccess.UnitOfWork;
using RC.Bll.Api.Services.Messages;

namespace RC.Bll.Api.Helpers
{
    public static class DiModules
    {
        public static IServiceCollection RegisterApiServices(this IServiceCollection services)
        {
            services.AddTransient<ICurrentAccount, CurrentAccount>();

            services.AddTransient<IRcAuthentication, RcAuthentication>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IMessageService, MessageService>();

            return services;
        }

        public static IServiceCollection RegisterUnitOfWork(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
