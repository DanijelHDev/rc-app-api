﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Model;
using System;

namespace RC.Bll.Api.Helpers
{
    public static class Extensions
    {
        public static bool IsValidGuid(this Guid? uuid)
        {
            if (uuid == null || uuid == Guid.Empty)
                return false;

            return true;
        }

        public static string ToJson(this object obj)
        {
            try
            {
                var json = JsonConvert.SerializeObject(obj);
                return json;
            }
            catch (Exception ex)
            {
                return $"Error happened while serializing! {ex.Message}";
            }
        }

        public static long ToUnixEpochDate(this DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }

    public static class StartupExtensions
    {
        public static IServiceCollection AddApiSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "RC",
                    Description = "RC Api",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Danijel Hrček",
                        Email = "danijelhdev@gmail.com",
                        Url = "http://danijelhrcek.com"
                    }
                });
            });

            return services;
        }
    }
}
