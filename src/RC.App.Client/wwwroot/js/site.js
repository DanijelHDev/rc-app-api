﻿$(document).ready(function () {

    $("select.autocomplete-dropdown").select2();

    // Notification badge
    window.setInterval(function () {

        var container = $(".notification-container");
        var action = container.data('action');

        $.ajax({
            url: action,
            method: "GET",
            success: function (response) {
                container.html(response);
            }
        });

    }, 5000);

    // Loader
    function showLoader() {
        var loaderWrapper = $(".loader-wrapper");
        if (loaderWrapper.hasClass("hide")) {
            loaderWrapper.removeClass("hide");
        }
    }

    function hideLoader() {
        var loaderWrapper = $(".loader-wrapper");
        if (!loaderWrapper.hasClass("hide")) {
            loaderWrapper.addClass("hide");
        }
    }

    $(window).scroll(function (e) {
        var scrollTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var scrollPercent = scrollTop / (docHeight - winHeight);
        var scrollPercentRounded = Math.round(scrollPercent * 100);

        if (scrollPercentRounded > 90) {
            $(document).find('.btn-more').trigger("click");
        }
    });

    $(document).find('#filter-form').on('submit', function (e) {
        showLoader();
        e.preventDefault();
        var form = $(this);
        var dataAjax = $(document).find(".data-ajax");

        form.find('input[name="Filter.Page"]').val(0);
        form.find('input[name="Filter.IsFilterClicked"]').val(true);

        $.ajax({
            url: form.attr("action"),
            method: form.attr("method"),
            data: form.serialize(),
            success: function (response) {
                dataAjax.html(response);
                hideLoader();
            }
        });
    });

    $(document).on("click", ".btn-delete", function () {

        if (confirm('Delete selected meessages?')) {
            var checkedArray = [];

            $(".message-list-wrapper input:checked").each(function () {
                checkedArray.push($(this).val());
            });

            $.ajax({
                url: $(this).attr("data-action"),
                method: 'POST',
                data: { 'ids': checkedArray },
                success: function () {
                    alert('Deleted');
                    $(".message-list-wrapper input:checked").each(function () {
                        $(this).closest(".single-message-wrapper").remove();
                    });
                }
            });
        }
    });

    $('.select-all').on('click', function () {

        var checkboxes = $('.delete-checkbox');

        var self = $(this);
        var allChecked = true;

        if (self.hasClass('checked-all')) {
            self.removeClass('checked-all');
            allChecked = false;
        } else {
            self.addClass('checked-all');
        }

        checkboxes.prop('checked', allChecked);
    });

    // Load more
    function loadMore() {
        var $moreBtn = $(document).find(".btn-more");

        $(document).on("click", ".btn-more", function () {
            showLoader();

            var form = $(document).find('#filter-form');

            if (form.find('input[name="Filter.IsFilterClicked"]').val() == "true") {
                var inputPage = form.find('input[name="Filter.Page"]');
                inputPage.val(Number(inputPage.val()) + 1);
            }

            var btn = $(this);
            var form = $(document).find('#filter-form');
            var dataAjax = $(document).find(".data-ajax");
            btn.parents(".load-more-parent").remove();
            $.ajax({
                url: form.attr("action"),
                method: form.attr("method"),
                data: form.serialize(),
                success: function (response) {
                    dataAjax.append(response);

                    var inputPage = $(document).find('#filter-form').find('input[name="Filter.Page"]');
                    inputPage.val(Number(inputPage.val()) + 1);

                    var inputIsFilterClicked = $(document).find('#filter-form').find('input[name="Filter.IsFilterClicked"]');
                    inputIsFilterClicked.val(Boolean(false));

                    hideLoader();
                }
            });
        });
    }

    loadMore();

});