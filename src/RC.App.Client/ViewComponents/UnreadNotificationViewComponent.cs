﻿using Microsoft.AspNetCore.Mvc;
using RC.Bll.Client.Services.Messages;
using System.Threading.Tasks;

namespace RC.App.Client.ViewComponents
{
    public class UnreadNotificationViewComponent : ViewComponent
    {
        private readonly IMessageService _messageService;

        public UnreadNotificationViewComponent(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var count = await _messageService.GetUnreadMessageCount();

            return View(count);
        }
    }
}
