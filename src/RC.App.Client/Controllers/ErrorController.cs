﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using static RC.Bll.Common.Helpers.Constants;

namespace RC.App.Client.Controllers
{
    public class ErrorController : BaseController
    {
        [AllowAnonymous]
        [HttpGet("/error/{statusCode}")]
        public IActionResult ErrorRoute(string statusCode)
        {
            if (statusCode == "404")
            {
                return View(statusCode);
            }

            if (statusCode == "401" || statusCode == "302")
            {
                return View(statusCode);
            }

            if (statusCode == "500")
            {
                return View(statusCode);
            }

            return View("~/Views/Error/404.cshtml");
        }
    }
}
