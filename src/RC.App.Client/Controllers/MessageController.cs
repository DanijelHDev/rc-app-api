﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Client.Models.ViewModels;
using RC.Bll.Client.Services.Messages;
using RC.Bll.Client.Models.Filter;
using System.Collections.Generic;
using System.Linq;

namespace RC.App.Client.Controllers
{
    public class MessageController : BaseController
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public async Task<IActionResult> Index()
        {
            var messages = await _messageService.GetAll(await _messageService.InitializeFilterFormValues());
            return View(messages);
        }

        public async Task<IActionResult> List(MessageFilter filter)
        {
            return PartialView("_List", await _messageService.GetAll(filter));
        }

        [HttpGet]
        public IActionResult Send(Guid toAccountId)
        {
            return View(new MessageVm() { ToAccountId = toAccountId });
        }

        [HttpPost]
        public async Task<IActionResult> Send(MessageVm vieModel)
        {
            if (!ModelState.IsValid)
                return View(vieModel);

            await _messageService.Send(vieModel);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteMessages(List<Guid> ids)
        {
            if (!ids.Any())
                return BadRequest();

            await _messageService.DeleteMessages(ids);
            return Ok();
        }
    }
}
