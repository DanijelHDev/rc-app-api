﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Client.Models.ViewModels;
using RC.Bll.Client.Services.Accounts;

namespace RC.App.Client.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IAccountService _accountService;

        public RegisterController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public IActionResult Index()
        {
            return View(new RegisterVm());
        }

        [HttpPost]
        public async Task<IActionResult> Index(RegisterVm model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var response = await _accountService.RegisterNew(model);
            if (!response.IsSuccess)
            {
                ViewData["RegisterError"] = response.ErrorMessage;
                return View();
            }

            return RedirectToAction("Index", "Login");
        }
    }
}
