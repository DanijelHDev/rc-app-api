﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Client.Services.Accounts;
using RC.Bll.Client.Models.Filter;

namespace RC.App.Client.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<IActionResult> Index()
        {
            var accounts = await _accountService.GetAll(new AccountFilter());

            return View(accounts);
        }

        public async Task<IActionResult> List(AccountFilter filter)
        {
            return PartialView("_List", await _accountService.GetAll(filter));
        }
    }
}
