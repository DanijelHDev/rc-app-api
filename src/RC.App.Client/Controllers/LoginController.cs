﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RC.Bll.Client.Services.Authentication;
using Microsoft.AspNetCore.Authorization;
using RC.Bll.Client.Models.ViewModels;

namespace RC.App.Client.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IRcAuthentication _authenticationService;

        public LoginController(IRcAuthentication authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        public IActionResult Index(string returnUrl)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Account");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Index(LoginVm model)
        {
            var authResult = await _authenticationService.SignIn(model.Username, model.Password, model.RememberMe);

            if (!authResult.IsSuccess)
            {
                ViewData["LoginError"] = authResult.ErrorMessage;
                return View();
            }

            ViewData["LoginError"] = String.Empty;

            // If there's data about return url sent
            if (!String.IsNullOrEmpty(model.ReturnUrl))
                return Redirect(model.ReturnUrl);

            return RedirectToAction("Index", "Account");
        }

        [HttpGet]
        [Authorize]
        public virtual async Task<ActionResult> Logout(LoginVm model)
        {
            await _authenticationService.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}
